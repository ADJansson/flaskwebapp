F�rste utfordring: installere python+flask. Det gikk ganske greit. Bruker Fiddler til testing

Neste steg: F� til GET med input (json-string) og sjekk at det kommer frem
Bruker GET fordi man forventer et svar (det nye polygonet) i samsvar med REST-tankegang
(POST kunne ogs� blitt brukt, men dette assosierer jeg med � opprette et persistent objekt i en database e.l.
PUT og DELETE er ikke relevante)

> Finn ut av geojson, debug koordinater p� server og sjekk at de er riktige
Leser i dokumentasjonen og finner de ulike medlemsvariablene, printer de ut. Alt ser OK ut
Neste steg: Iterer over features i FeatureCollection og finn koordinatene, og send de tilbake til klienten. 2-veis OK

> Finne et bibliotek som kan regne ut intersection mellom polygoner
	> Shapely ser bra ut og st�tter geojson

> Konverter til shapely-objekter, regner ut intesection, bruker .bounds for � finne bounds. Lager et kvadrat av dette.
> Sender det tilbake til klient og sjekker at koordinatene er riktige
> Bruker Feature(geometry = ) for � konvertere til geojson-format og returnerer dette
> Lager en for-l�kke for � regne ut intersection mellom flere polygoner
> Lager en if-test hvis man f�r inn ett polygon

bruker
https://blog.miguelgrinberg.com/post/designing-a-restful-api-with-python-and-flask
og
https://github.com/frewsxcv/python-geojson

fiddler  test-string: 
http://127.0.0.1:5000/geoApp/%7B%22features%22%3A%5B%7B%22geometry%22%3A%7B%22coordinates%22%3A%5B%5B%5B0.0%2C2.0%5D%2C%5B2.0%2C0.0%5D%2C%5B4.0%2C2.0%5D%2C%5B2.0%2C4.0%5D%2C%5B0.0%2C2.0%5D%5D%5D%2C%22type%22%3A%22Polygon%22%7D%2C%22properties%22%3A%7B%7D%2C%22type%22%3A%22Feature%22%7D%2C%7B%22geometry%22%3A%7B%22coordinates%22%3A%5B%5B%5B2.0%2C2.0%5D%2C%5B4.0%2C0.0%5D%2C%5B6.0%2C2.0%5D%2C%5B4.0%2C4.0%5D%2C%5B2.0%2C2.0%5D%5D%5D%2C%22type%22%3A%22Polygon%22%7D%2C%22properties%22%3A%7B%7D%2C%22type%22%3A%22Feature%22%7D%5D%2C%22type%22%3A%22FeatureCollection%22%7D

http://127.0.0.1:5000/geoApp/%7B%22features%22%3A%5B%7B%22geometry%22%3A%7B%22coordinates%22%3A%5B%5B%5B0.0%2C2.0%5D%2C%5B2.0%2C0.0%5D%2C%5B4.0%2C2.0%5D%2C%5B2.0%2C4.0%5D%2C%5B0.0%2C2.0%5D%5D%5D%2C%22type%22%3A%22Polygon%22%7D%2C%22properties%22%3A%7B%7D%2C%22type%22%3A%22Feature%22%7D%5D%2C%22type%22%3A%22FeatureCollection%22%7D
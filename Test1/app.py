#!flask/bin/python
from flask import Flask, request
from shapely.geometry import mapping, shape, box
import geojson
from geojson import Feature

app = Flask(__name__)

@app.route('/geoApp/<string:input>', methods=['GET'])
def get_response(input):
	collection = geojson.loads(input)
	
	# declare variables
	isect = None
	result = None
	
	# one polygon
	if len(collection.features) == 1:
		a = shape(collection.features[0].geometry)
		result = make_feature(get_bounding_box(a.bounds))
	elif len(collection.features) > 1:
	# support 2+ polygons
		for i in range(len(collection.features) - 1):
			if i > 0: #following iterations++
				a = isect
				b = shape(collection.features[i + 1].geometry)	
			else: # first iteration
				a = shape(collection.features[i].geometry)
				b = shape(collection.features[i + 1].geometry)	
			isect = a.intersection(b)
		result = make_feature(get_bounding_box(isect.bounds))
		
	return str(result)

#helper functions
def make_feature(shap):
	return Feature(geometry = shap)
	
def get_bounding_box(bounds):
	return mapping(box(bounds[0], bounds[1], bounds[2], bounds[3]))
	
if __name__ == '__main__':
    app.run(debug=True)